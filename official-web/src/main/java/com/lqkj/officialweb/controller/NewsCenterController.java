package com.lqkj.officialweb.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lqkj.officialweb.po.TNewsCenter;
import com.lqkj.officialweb.service.NewsCenterService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("newsCenter")
public class NewsCenterController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private NewsCenterService newsCenterService;

    /**
     *
     * @param searchModel
     * @param page
     * @return
     */
    @GetMapping("list")
    public R selectList(TNewsCenter searchModel, Page<Map<String,Object>> page){
        return success(newsCenterService.getList(page,searchModel));
    }
    @GetMapping("newestList")
    public R selectNewestList(TNewsCenter searchModel, Page<Map<String,Object>> page){
        return success(newsCenterService.getNewestList(page,searchModel));
    }
    /**
     * 分页查询所有数据
     *
     * @param page 分页对象CoreBusinessDao
     * @param tNewsCenter 查询实体
     * @return 所有数据
     */
    @GetMapping
    public R selectAll(Page<TNewsCenter> page, TNewsCenter tNewsCenter) {
        return success(this.newsCenterService.page(page, new QueryWrapper<>(tNewsCenter)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(this.newsCenterService.getById(id));
    }

    /*
     * 新增数据
     *
     * @param product 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody TNewsCenter tNewsCenter) {
        boolean rs = this.newsCenterService.save(tNewsCenter);
        return success(rs ? tNewsCenter:null);
    }

    /**
     * 修改数据
     *
     * @param tNewsCenter 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R update(@RequestBody TNewsCenter tNewsCenter) {
        boolean rs = this.newsCenterService.updateById(tNewsCenter);
        return success(rs ? tNewsCenter : null );
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestParam("idList") List<String> idList) {
        return success(this.newsCenterService.removeByIds(idList));
    }
    /**
     * 查询所有新闻内容
     * @return 所有新闻内容
     */
    @GetMapping("/getNewsAll")
    public R getNewsAll() {
        return success(this.newsCenterService.getNewsAll());
    }

}
