package com.lqkj.officialweb.controller;

import com.lqkj.officialweb.response.ResultModel;
import com.lqkj.officialweb.service.PositionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @Auther: lgf
 * @Date: 2020/5/8
 * @Description: 特别提醒，po的实体类先不要跟数据哭对应，还有xml，最好这两部分重写。
 * @version: 1.0
 */
@RestController
@Slf4j
@RequestMapping("/position")
public class PositionController {

    @Resource
    private PositionService positionService;

    /**
     * 得到每个职位的职责，资格和职位诱惑
     * @param positionCode //职责编码
     * @return 职责，资格和职位诱惑的封装类
     */
    @GetMapping("/dutyAndRequireAndPosition/{positionCode}")
    public ResultModel getDutyAndRequireAndPosition(@PathVariable("positionCode") Integer positionCode){
        return positionService.getDutyAndRequireAndPosition(positionCode);
    }
}
