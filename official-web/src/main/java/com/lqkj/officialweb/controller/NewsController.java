package com.lqkj.officialweb.controller;

import com.lqkj.officialweb.po.News;
import com.lqkj.officialweb.response.ResultModel;
import com.lqkj.officialweb.service.NewsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: lgf
 * @Date: 2020/5/9
 * @Description: 新闻动态
 * @version: 1.0
 */
@RestController
@Slf4j
@RequestMapping("/news")
public class NewsController {

    @Resource
    private NewsService newsService;

    /**
     * 获取所有的新闻信息(content内容除外)
     * @return 所有的新闻信息(content内容除外)
     */
    @GetMapping("/findAll")
    public ResultModel findAll(){
        return newsService.selectAllNews();
    }

    /**
     * 根据id获取新闻内容
     * @param id 新闻的唯一标识
     * @return 一条新闻信息
     */
    @GetMapping("/findNewsById")
    public ResultModel findNewsById(@RequestParam int id) {
        Map<String,Object> resultMap = new HashMap<String,Object>();
        resultMap = newsService.findNewsById(id);
        return ResultModel.successModel(resultMap);
    }
}
