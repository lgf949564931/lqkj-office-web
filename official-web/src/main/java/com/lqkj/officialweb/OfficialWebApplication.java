package com.lqkj.officialweb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("com.lqkj.officialweb.dao.mapper")
@EnableTransactionManagement //开启事务控制
public class OfficialWebApplication extends SpringBootServletInitializer
{
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        return application.sources(new Class[] { OfficialWebApplication.class });
    }
    public static void main(String[] args) {

        SpringApplication.run(OfficialWebApplication.class, args);
    }

}