package com.lqkj.officialweb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lqkj.officialweb.dao.mapper.ResourceTypeDao;
import com.lqkj.officialweb.po.ResourceType;
import com.lqkj.officialweb.service.ResourceTypeService;
import org.springframework.stereotype.Service;

/**
 * (ResourceType)表服务实现类
 * 该类由EasyCode工具生成
 * @author 小明哥
 * @since 2020-03-15 22:49:39
 */
@Service("resourceTypeService")
public class ResourceTypeServiceImpl extends ServiceImpl<ResourceTypeDao, ResourceType> implements ResourceTypeService {

}