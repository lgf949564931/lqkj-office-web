package com.lqkj.officialweb.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lqkj.officialweb.po.TNewsCenter;

import java.util.List;
import java.util.Map;

public interface NewsCenterService extends IService<TNewsCenter> {
    /**
     *新闻内容
     * @param page
     * @param seachModel
     * @return
     */
    Page<Map<String,Object>> getList(Page<Map<String, Object>> page, TNewsCenter seachModel);
    /**
     *最新新闻内容
     * @param page
     * @param seachModel
     * @return
     */
    Page<Map<String,Object>> getNewestList(Page<Map<String, Object>> page, TNewsCenter seachModel);

    /**
     * 查询所有新闻内容
     * @return 所有新闻内容
     */
    List<Map<String, Object>> getNewsAll();
}
