package com.lqkj.officialweb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lqkj.officialweb.dao.mapper.NewsBannerDao;
import com.lqkj.officialweb.enums.IsUsedEnum;
import com.lqkj.officialweb.po.TNewsBanner;
import com.lqkj.officialweb.service.NewsBannerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("NewsBannerService")
public class NewsBannerServiceImpl extends ServiceImpl<NewsBannerDao, TNewsBanner>  implements NewsBannerService {

    //打印信息
    Logger logger =  LoggerFactory.getLogger (this.getClass ());

    /**
     * 代码实现查询结果分页
     * @param page
     * @param searchModel
     * @return
     */
    @Override
    public Page<Map<String, Object>> getList(Page<Map<String, Object>> page, TNewsBanner searchModel) {



        //名称
        String  coreBusinessTitle  = searchModel.getNewsBannerTitle();
        //是否启用
        IsUsedEnum isUsed = searchModel.getIsUsed();
        Integer isuse = -1;
        if(StringUtils.checkValNotNull(isUsed)) {
            isuse = isUsed.getValue ();
        }
        //查询条件
        LambdaQueryWrapper<TNewsBanner> lambdaQueryWrapper = new LambdaQueryWrapper ();
        //匹配查询结条件
        lambdaQueryWrapper
                .eq (
                        //1.是否使用
                        StringUtils.checkValNotNull (isuse) && isuse>=0,
                        TNewsBanner::getIsUsed,
                        isuse)
                .like (
                        //4.模糊查询商品名称
                        StringUtils.isNotEmpty (coreBusinessTitle),
                        TNewsBanner::getNewsBannerTitle,
                        coreBusinessTitle);

        //分页对象是否为空
        if(page==null) {
            page = new Page<> ();
        }

        //结果
        List<Map<String, Object>> list = this.baseMapper.getList (page,lambdaQueryWrapper);

        return page.setRecords (list);
    }
}
