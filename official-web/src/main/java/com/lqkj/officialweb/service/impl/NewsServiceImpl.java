package com.lqkj.officialweb.service.impl;

import com.lqkj.officialweb.dao.mapper.NewsInformationMapper;
import com.lqkj.officialweb.dao.mapper.NewsMapper;
import com.lqkj.officialweb.po.News;
import com.lqkj.officialweb.po.TNewsInformation;
import com.lqkj.officialweb.response.ResultModel;
import com.lqkj.officialweb.service.NewsService;
import com.lqkj.officialweb.vo.QueryVoCount;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: lgf
 * @Date: 2020/5/9
 * @Description: com.ke.sm.service.impl
 * @version: 1.0
 */
@Service
public class NewsServiceImpl implements NewsService {

    @Resource
    private NewsMapper newsMapper;
    @Resource
    private NewsInformationMapper newsInformationMapper;

    @Override
    public ResultModel selectAllNews() {
        List<News> newsList = newsMapper.selectAllNews();
        return ResultModel.successModel(newsList);
    }

    @Override
    public  Map<String,Object> findNewsById(int id) {
        News news = newsMapper.selectByPrimaryKey(id);
        List<TNewsInformation> newsInformationList =  newsInformationMapper.selectByNewsInformation(id);
        Map<String,Object> resultMap = new HashMap<String,Object>();
        resultMap.put("news",news);
        resultMap.put("newsInformationList",newsInformationList);
        return resultMap;
    }
}
