package com.lqkj.officialweb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lqkj.officialweb.po.ResourceType;

/**
 * (ResourceType)表服务接口
 * 该类由EasyCode工具生成
 * @author 小明哥
 * @since 2020-03-15 22:49:39
 */
public interface ResourceTypeService extends IService<ResourceType> {

}