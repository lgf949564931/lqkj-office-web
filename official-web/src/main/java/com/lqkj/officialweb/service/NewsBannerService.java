package com.lqkj.officialweb.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lqkj.officialweb.po.TNewsBanner;

import java.util.Map;

public interface NewsBannerService extends IService<TNewsBanner> {
    /**
     * 新闻动态（内容展示，带搜索）
     * @param page
     * @param searchModel
     * @return
     */
    Page<Map<String,Object>> getList(Page<Map<String, Object>> page, TNewsBanner searchModel);

}
