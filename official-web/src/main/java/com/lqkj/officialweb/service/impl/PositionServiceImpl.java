package com.lqkj.officialweb.service.impl;

import com.lqkj.officialweb.dao.mapper.DutyMapper;
import com.lqkj.officialweb.dao.mapper.PositionMapper;
import com.lqkj.officialweb.dao.mapper.RequiredMapper;
import com.lqkj.officialweb.po.Duty;
import com.lqkj.officialweb.po.Position;
import com.lqkj.officialweb.po.Required;
import com.lqkj.officialweb.response.ResultModel;
import com.lqkj.officialweb.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther: lgf
 * @Date: 2020/5/8
 * @Description: com.ke.sm.service.impl
 * @version: 1.0
 */
@Service
public class PositionServiceImpl implements PositionService {

    @Resource
    private PositionMapper positionMapper;

    @Autowired
    private RequiredMapper requiredMapper;

    @Resource
    private DutyMapper dutyMapper;

    @Override
    public List<Position> findAllPosition(int pageSize, int currPage) {
        return positionMapper.findAllPosition(pageSize, currPage);
    }

    @Override
    public  List<Position> selectByPrimaryKey(Integer id) {
        return positionMapper.selectByPrimaryKey(id);
    }



    /**
     * 将职责，资格和职位诱惑封装在ResultModel中
     * @param positionCode
     * @return ResultModel
     */

    @Override
    public ResultModel getDutyAndRequireAndPosition(Integer positionCode)
    {
        List<Duty> dutiesList = dutyMapper.selectDutyByPositionCode(positionCode);

        List<Required> requiredList = requiredMapper.selectRequiredByPositionCode(positionCode);

        List<Position> position = positionMapper.selectPsoitionByPsoitionCode(positionCode);

        Map<String,Object> map = new HashMap();
        map.put("position",position.get(0).getPositionName());

        map.put("duty",dutiesList);

        map.put("required",requiredList);

        return ResultModel.successModel(map);
    }
}
