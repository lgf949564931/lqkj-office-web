package com.lqkj.officialweb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lqkj.officialweb.dao.mapper.NewsCenterDao;
import com.lqkj.officialweb.enums.IsUsedEnum;
import com.lqkj.officialweb.po.TNewsCenter;
import com.lqkj.officialweb.service.NewsCenterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("NewsCenterService")
public class NewsCenterServiceImpl extends ServiceImpl<NewsCenterDao, TNewsCenter> implements NewsCenterService {
    //打印信息
    Logger logger =  LoggerFactory.getLogger (this.getClass ());
    @Override
    public Page<Map<String, Object>> getList(Page<Map<String, Object>> page, TNewsCenter searchModel) {
        //名称
        String  coreBusinessTitle  = searchModel.getNewsCenterTitle();
        //是否启用
        IsUsedEnum isUsed = searchModel.getIsUsed();
        Integer newsType = searchModel.getNewsType();
        Integer isuse = -1;
        if(StringUtils.checkValNotNull(isUsed)) {
            isuse = isUsed.getValue ();
        }
        Integer newstype = -1;
        if(StringUtils.checkValNotNull(newsType)) {
            newstype = newsType;
        }
        //查询条件
        LambdaQueryWrapper<TNewsCenter> lambdaQueryWrapper = new LambdaQueryWrapper ();
        //匹配查询结条件
        lambdaQueryWrapper
                .eq (
                        //1.是否使用
                        StringUtils.checkValNotNull (isuse) && isuse>=0,
                        TNewsCenter::getIsUsed,
                        isuse)
                .like (
                        StringUtils.isNotEmpty (coreBusinessTitle),
                        TNewsCenter::getNewsCenterTitle,
                        coreBusinessTitle)
                .eq (
                        StringUtils.checkValNotNull (newstype) && newstype>=0,
                        TNewsCenter::getNewsType,
                        newstype);
        //分页对象是否为空
        if(page==null) {
            page = new Page<> ();
        }

        //结果
        List<Map<String, Object>> list = this.baseMapper.getList (page,lambdaQueryWrapper);

        return page.setRecords (list);
    }

    @Override
    public Page<Map<String, Object>> getNewestList(Page<Map<String, Object>> page, TNewsCenter seachModel) {
        Integer isNewest = 1;
        //查询条件
        LambdaQueryWrapper<TNewsCenter> lambdaQueryWrapper = new LambdaQueryWrapper ();
        //匹配查询
        lambdaQueryWrapper
                .eq (

                        TNewsCenter::getIsNewest,
                        isNewest);
        List<Map<String, Object>> newestList = this.baseMapper.getNewestList(page, lambdaQueryWrapper);
        return page.setRecords(newestList);
    }


    /**
     * 查询所有新闻内容
     * @return 所有新闻内容
     */
    @Override
    public List<Map<String, Object>> getNewsAll() {
        return this.baseMapper.getNewsAll();
    }

}
