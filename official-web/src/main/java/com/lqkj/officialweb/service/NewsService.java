package com.lqkj.officialweb.service;

import com.lqkj.officialweb.response.ResultModel;

import java.util.List;
import java.util.Map;

/**
 * @Auther: lgf
 * @Date: 2020/5/9
 * @Description: com.ke.sm.service
 * @version: 1.0
 */
public interface NewsService {
    ResultModel selectAllNews();

    Map<String,Object> findNewsById(int id);
}
