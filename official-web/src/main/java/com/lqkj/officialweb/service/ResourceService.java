package com.lqkj.officialweb.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lqkj.officialweb.po.Resource;

import java.util.List;
import java.util.Map;

/**
 * (Resource)表服务接口
 * 该类由EasyCode工具生成
 * @author 小明哥
 * @since 2020-03-15 22:49:39
 */
public interface ResourceService extends IService<Resource> {
    List<Resource> getListByIds(String ids);
    /**
     * 图片列表（后台管理列表，带搜索）
     * @param searchModel
     * @return
     */
    Page<Map<String,Object>> getList(Page<Map<String, Object>> page, Resource searchModel);

}