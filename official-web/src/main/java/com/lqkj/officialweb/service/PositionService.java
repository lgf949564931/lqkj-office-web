package com.lqkj.officialweb.service;

import com.lqkj.officialweb.po.Duty;
import com.lqkj.officialweb.po.Position;
import com.lqkj.officialweb.po.Required;
import com.lqkj.officialweb.response.ResultModel;

import java.util.List;

/**
 * @Auther: lgf
 * @Date: 2020/5/8
 * @Description: com.ke.sm.service
 * @version: 1.0
 */
public interface PositionService {

    List<Position> findAllPosition(int pageSize, int currPage);

    List<Position> selectByPrimaryKey(Integer id);

//    Position selectPsoitionByPsoitionCode(Integer positionCode);
//
//    List<String> selectDutyByPositionCode(Integer positionCode);
//
  //  List<Required> selectRequiredByPositionCode(Integer positionCode);

    ResultModel getDutyAndRequireAndPosition(Integer positionCode);
}
