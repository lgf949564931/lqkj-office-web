package com.lqkj.officialweb.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lqkj.officialweb.dao.mapper.ResourceDao;
import com.lqkj.officialweb.enums.StatusModelEnum;
import com.lqkj.officialweb.po.Resource;
import com.lqkj.officialweb.service.ResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * (Resource)表服务实现类
 * 该类由EasyCode工具生成
 * @author 小明哥
 * @since 2020-03-15 22:49:39
 */
@Service("resourceService")
public class ResourceServiceImpl extends ServiceImpl<ResourceDao, Resource> implements ResourceService {


    private Logger log   = LoggerFactory.getLogger (this.getClass ());

    /**
     *
     * @param ids
     * @return
     */
    public List<Resource> getListByIds(String ids){

        List<Resource> list = new ArrayList<>();

        if(StringUtils.isNotEmpty(ids)) {

            String[] idList = ids.split (",");

            LambdaQueryWrapper<Resource> queryWrapper = new QueryWrapper().lambda ();

            queryWrapper.in (StringUtils.isNotEmpty (ids), Resource::getId, Arrays.asList(idList));

            list = this.list (queryWrapper);

        }

        //log.info ("service list==>" + list.toString ());

        return list;

    }

    @Override
    public Page<Map<String, Object>> getList(Page<Map<String, Object>> page, Resource searchModel) {
        //销售模式
        StatusModelEnum statusModel = searchModel.getStatusModel();
        Integer stateModel = 0;
        if(StringUtils.checkValNotNull(statusModel)) {
            stateModel = statusModel.getValue ();
        }

        //图片类型
        Integer typeId = searchModel.getTypeId();


        //图片名称
        String  resourceName  = searchModel.getResourceName();

        //查询条件
        LambdaQueryWrapper<Resource> lambdaQueryWrapper = new LambdaQueryWrapper ();
        //匹配查询结条件
        lambdaQueryWrapper

                //1.资源类型
                .eq (
                        StringUtils.checkValNotNull (typeId) && typeId>0,
                        Resource::getTypeId,
                        typeId)
                .eq (
                        //2.匹配销售模式
                        StringUtils.checkValNotNull (stateModel) && stateModel>0,
                        Resource::getStatusModel,
                        stateModel)
                .like (
                        //3.模糊查询资源名称
                        StringUtils.isNotEmpty (resourceName),
                        Resource::getResourceName,
                        resourceName);

        //分页对象是否为空
        if(page==null) {
            page = new Page<> ();
        }

        //结果
        List<Map<String, Object>> list = this.baseMapper.getList (page,lambdaQueryWrapper);

        return page.setRecords (list);
    }

}