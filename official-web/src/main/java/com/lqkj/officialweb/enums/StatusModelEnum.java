package com.lqkj.officialweb.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @Auther: ligaofeng
 * @Date: 2020/9/7
 * @Description: com.lqkj.officialweb.enums
 * @email: 949564931@qq.com
 * @version: 1.0
 */
public enum StatusModelEnum implements IEnum {
    STATUS_ONE(1, "启用"),
    STATUS_TWO(2, "停用");

    private int code;
    private String desc;

    StatusModelEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    @Override
    public Integer getValue() {

        return this.code;
    }

    public String getDesc() {
        return desc;
    }

    public String toString(){
        return desc;
    }

}
