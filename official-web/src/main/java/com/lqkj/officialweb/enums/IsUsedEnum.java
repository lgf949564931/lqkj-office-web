package com.lqkj.officialweb.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * 是否使用
 */

public enum IsUsedEnum  implements IEnum <Integer>{

    IS_USED_ONE(0, "否"),
    IS_USED_TWO(1, "是");


    private  int code;
    private String desc;

    IsUsedEnum(int code ,String desc){
        this.code = code ;
        this.desc = desc ;
    }
    @Override
    public Integer getValue() {
        return this.code;
    }
    public String getDesc() {
        return desc;
    }

    public String toString(){
        return desc;
    }
}
