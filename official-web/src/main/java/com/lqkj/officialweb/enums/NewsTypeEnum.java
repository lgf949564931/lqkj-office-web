package com.lqkj.officialweb.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

public enum NewsTypeEnum implements IEnum<Integer> {
    NEWS_TYPE_ONE(0, "公司新闻"),
    NEWS_TYPE_TWO(1, "行业动态"),
    NEWS_TYPE_THREE(2, "领导关怀");

    private  int code;
    private String desc;

    NewsTypeEnum(int code ,String desc){
        this.code = code ;
        this.desc = desc ;
    }
    @Override
    public Integer getValue() {
        return this.code;
    }
    public String getDesc() {
        return desc;
    }

    public String toString(){
        return desc;
    }
}
