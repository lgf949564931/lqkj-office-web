package com.lqkj.officialweb.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;

/**
 * @Auther: ligaofeng
 * @Date: 2020/10/10
 * @Description: com.lqkj.officialweb.enums
 * @email: 949564931@qq.com
 * @version: 1.0
 */
public enum IsNewestEnum implements IEnum<Integer> {
    IS_NEWEST_ONE(1, "最新"),
    IS_NEWEST_TWO(2, "历史");

    private int code;
    private String desc;

    IsNewestEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    @Override
    public Integer getValue() {

        return this.code;
    }

    public String getDesc() {
        return desc;
    }

    public String toString(){
        return desc;
    }

}
