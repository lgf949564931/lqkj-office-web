package com.lqkj.officialweb.response;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public final class ResultModel implements Serializable {



    public static final int RETURN_CODE_SUCCESS = 0;
    public static final int RETURN_CODE_FAIL = 1;

    private ResultModel(){
        code = RETURN_CODE_SUCCESS;
        data = null;
    }

    private ResultModel(int code, Object data) {
        this.code = code;
        this.data = data;
    }


    private int code = 0;
    private Object data;


    public void setCode(int code) {
        this.code = code;
    }


    public int getCode() {
        return code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    /**
     * 构造返回模型
     *
     * @param flag
     * @return
     */
    public static ResultModel buildModel(boolean flag) {
        if (flag) {
            return successModel();
        }
        return failModel();
    }

    /**
     * 获取成功的 ResultModel
     *
     * @return
     */
    public static ResultModel successModel() {
        return successModel("ok");
    }

    /**
     * 获取成功的 ResultModel
     *   
     * @return
     */
    public static ResultModel successModel(Object data) {
        return new ResultModel(RETURN_CODE_SUCCESS, data);
    }
//    public static ResultModel successModel(String cityId,String cityName,Object data) {
//        return new ResultModel(RETURN_CODE_SUCCESS,cityId,cityName, data);
//    }
    /**
     * 获取失败的 ResultModel
     *
     * @return
     */
    public static ResultModel failModel() {
        return failModel("失败");
    }

    /**
     * 获取失败的 ResultModel
     *
     * @return
     */
    public static ResultModel failModel(Object data) {
        return new ResultModel(RETURN_CODE_FAIL, data);
    }

    public static ResultModel successModel(String catalogoName, Object data) {
        Map jobj = new HashMap();
        jobj.put(catalogoName,data);
        return new ResultModel(RETURN_CODE_SUCCESS, jobj);
    }
    public static ResultModel successModel(String catalogoName, Object data, String cityId, String cityName) {
        Map jobj = new HashMap();
        jobj.put(catalogoName,data);
        jobj.put(catalogoName,cityId);
        jobj.put(catalogoName,cityName);
        return new ResultModel(RETURN_CODE_SUCCESS, jobj);
    }
}
