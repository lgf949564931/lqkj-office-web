package com.lqkj.officialweb.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TNewsInformation {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 新闻编码
     */
    private Integer newsCode;

    /**
     * 新闻图片
     */
    private String newsImg;

    /**
     * 新闻详细内容
     */
    private String newsInfo;

    /**
     * 备注
     */
    private String remark;
}

