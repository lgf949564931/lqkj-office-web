package com.lqkj.officialweb.po;

import lombok.Data;

import java.util.Date;

/**
 * news
 * @author LENOVO
 * @date 2020-05-13 15:47:27
 */
@Data
public class News {
    /**
     * ID
     */
    private Integer id;

    /**
     * 新闻标题
     */
    private String title;

    /**
     * 图片地址
     */
    private String img;

    /**
     * 新闻报道时间
     */
    private Date recordTime;

    /**
     * 新闻内容
     */
    private String content;
    /**
     * 新闻类型
     */
    private Integer newsType;
}