package com.lqkj.officialweb.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: zhen
 * @CreateTime: 2020/8/13 13:08
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Required implements Serializable
{
    /**
     * ID
     */
    private Integer id;

    /**
     * 技能要求
     */
    private String positionRequired;

    /**
     * 技能编码
     */
    private String positionCode;

}
