package com.lqkj.officialweb.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: zhen
 * @CreateTime: 2020/8/13 13:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Duty implements Serializable
{
    /**
     * ID
     */
    private Integer id;

    /**
     * 职位要求
     */
    private String positionDuty;

    /**
     * 职位编码
     */
    private Integer positionCode;



}
