package com.lqkj.officialweb.po;

import lombok.Data;

import java.util.List;

/**
 * position
 * @author LENOVO
 * @date 2020-05-08 14:58:18
 */
@Data
public class Position {
    /**
     * ID
     */
    private Integer id;

    /**
     * 显示优先级
     */
    private Integer positionCode;

    /**
     * 职位名称
     */
    private String positionName;

    /**
     * 福利
     */
    private String welfare;


    /**
     * 逻辑删除
     */
    private Integer status;
}