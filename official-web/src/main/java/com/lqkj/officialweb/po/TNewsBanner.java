package com.lqkj.officialweb.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.lqkj.officialweb.enums.IsUsedEnum;
import lombok.Data;

import java.util.Date;

@Data
@SuppressWarnings("serial")
public class TNewsBanner extends Model<TNewsBanner> {
    /**
     * 主键
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    /**
     * 是否使用
     */
    @TableId(value = "is_used")
    private IsUsedEnum isUsed;

    /**
     * 新闻动态标题图片id
     */
    private String newsBannerImgId;

    /**
     * 新闻动态标题图片路径
     */
    private String newsBannerImgUrl;

    /**
     * 新闻动态标题文字
     */
    private String newsBannerTitle;

    /**
     * 新闻标题内容
     */
    private String newsBannerDetails;

    /**
     * 备注
     */
    private String remark;

    /**
     * 修改人
     */
    private String modifUser;

    /**
     * 修改日期
     */
    private Date modifDate;

    /**
     * 备注字段1
     */
    private String field1;

    /**
     * 备注字段2
     */
    private String field2;

    /**
     * 备注字段3
     */
    private String field3;
}

