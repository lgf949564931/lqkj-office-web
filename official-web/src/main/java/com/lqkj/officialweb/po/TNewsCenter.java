package com.lqkj.officialweb.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.lqkj.officialweb.enums.IsNewestEnum;
import com.lqkj.officialweb.enums.IsUsedEnum;
import lombok.Data;

import java.util.Date;

@Data
@SuppressWarnings("serial")
public class TNewsCenter extends Model<TNewsCenter> {
    /**
     * 主键
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    /**
     * 新闻内容标题
     */
    private String newsCenterTitle;

    /**
     * 新闻内容图片id
     */
    private String newsCenterImgId;

    /**
     * 新闻图片路径
     */
    private String newsCenterImgUrl;

    /**
     * 新闻详细内容
     */
    private String newsContent;

    /**
     * 新闻报道时间
     */
    private Date recordTime;

    /**
     * 是否使用 0，否；1，是；
     */
    @TableId(value = "is_used")
    private IsUsedEnum isUsed;

    /**
     * 0.公司新闻；1.行业动向 ；2.领导关怀；
     */
    private Integer newsType;

    /**
     * 修改人
     */
    private String modifUser;

    /**
     * 修改时间
     */
    private String modifDate;

    /**
     * 备注字段1
     */
    private String field1;

    /**
     * 备注字段2
     */
    private String field2;

    /**
     * 最新
     */
    @TableId(value = "is_newest")
    private IsNewestEnum isNewest;
}

