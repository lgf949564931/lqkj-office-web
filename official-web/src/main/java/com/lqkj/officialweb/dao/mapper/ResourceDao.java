package com.lqkj.officialweb.dao.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.lqkj.officialweb.po.Resource;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * (Resource)表数据库访问层
 * 该类由EasyCode工具生成
 * @author 小明哥
 * @since 2020-03-15 22:49:39
 */
public interface ResourceDao extends BaseMapper<Resource> {
    /**
     * 关联多张表进行查询
     * @param page 分页
     * @param queryWrapper 查询条件
     * @return
     */
    @Select("select\n" +
            "       a.*, \n" +
            "       case \n"+
            "         when a.resource_state=1 then '启用' \n"+
            "         when a.resource_state=2 then '停用' \n"+
            "       end stateModel,\n"+
            "       b.resource_type_name\n" +
            "from resource a\n" +
            "  left join resource_type b on b.id = a.type_id\n" +
            "${ew.customSqlSegment}")
    public List<Map<String, Object>> getList(IPage<Map<String, Object>> page, @Param(Constants.WRAPPER) Wrapper<Resource> queryWrapper);
}