package com.lqkj.officialweb.dao.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.lqkj.officialweb.po.TNewsCenter;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface NewsCenterDao extends BaseMapper<TNewsCenter> {
    @Select("SELECT\n" +
            "	t.id, \n" +
            "	t.news_center_img_url  , \n" +
            "	t.news_center_img_id , \n" +
            "	t.news_center_title , \n" +
            "	t.news_content , \n" +
            "	t.record_time , \n" +
//            "	CASE \n" +
//            "		WHEN t.news_type = '1' THEN  '公司新闻' \n" +
//            "		WHEN t.news_type = '2' THEN  '行业动态' \n" +
//            "       WHEN t.news_type = '3' THEN  '家乡情怀' \n" +
//            "   end newsType,	\n" +
            "   t.news_type as newsType, \n" +
            "	CASE \n" +
            "		WHEN t.is_newest = '1' THEN  '最新' \n" +
            "		WHEN t.is_newest = '2' THEN  '历史' \n" +
            "   end isNewest ,\n" +
            "	CASE \n" +
            "		WHEN t.is_used = '1' THEN  '是' \n" +
            "		WHEN t.is_used = '0' THEN  '否' \n" +
            "   end isUsed	\n" +
            "   FROM \n" +
            "	t_news_center t \n" +
            "${ew.customSqlSegment}"+
            "order by t.record_time desc "
    )
    public List<Map<String, Object>> getList(IPage<Map<String, Object>> page, @Param(Constants.WRAPPER) Wrapper<TNewsCenter> queryWrapper);
    @Select("SELECT\n" +
            "	t.id, \n" +
            "	t.news_center_img_url  , \n" +
            "	t.news_center_img_id , \n" +
            "	t.news_center_title , \n" +
            "	t.news_content , \n" +
            "	t.record_time , \n" +
//            "	CASE \n" +
//            "		WHEN t.news_type = '1' THEN  '公司新闻' \n" +
//            "		WHEN t.news_type = '2' THEN  '行业动态' \n" +
//            "       WHEN t.news_type = '3' THEN  '家乡情怀' \n" +
//            "   end newsType,	\n" +
            "   t.news_type as newsType, \n" +
            "	CASE \n" +
            "		WHEN t.is_newest = '1' THEN  '最新' \n" +
            "		WHEN t.is_newest = '2' THEN  '历史' \n" +
            "   end isNewest ,\n" +
            "	CASE \n" +
            "		WHEN t.is_used = '1' THEN  '是' \n" +
            "		WHEN t.is_used = '0' THEN  '否' \n" +
            "   end isUsed	\n" +
            "   FROM \n" +
            "	t_news_center t \n" +
            "${ew.customSqlSegment}"+
            "order by t.record_time desc "
    )
    public List<Map<String, Object>> getNewestList(IPage<Map<String, Object>> page, @Param(Constants.WRAPPER) Wrapper<TNewsCenter> queryWrapper);

    /**
     * 查询所有新闻数据
     * @return 所有数据
     */
    @Select("SELECT\n" +
            "  t.id, \n" +
            "  t.news_center_img_url  , \n" +
            "  t.news_center_img_id , \n" +
            "  t.news_center_title , \n" +
            "  t.news_content , \n" +
            "  t.record_time , \n" +
            "   t.news_type as newsType, \n" +
            "  CASE \n" +
            "     WHEN t.is_newest = '1' THEN  '最新' \n" +
            "     WHEN t.is_newest = '2' THEN  '历史' \n" +
            "   end isNewest ,\n" +
            "  CASE \n" +
            "     WHEN t.is_used = '1' THEN  '是' \n" +
            "     WHEN t.is_used = '0' THEN  '否' \n" +
            "   end isUsed \n" +
            "   FROM \n" +
            "  t_news_center t \n" +
            "order by t.record_time desc ")
    List<Map<String, Object>> getNewsAll();
}