package com.lqkj.officialweb.dao.mapper;

import com.lqkj.officialweb.po.Position;
import com.lqkj.officialweb.response.ResultModel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PositionMapper {

    List<Position> findAllPosition(int pageSize, int currPage);

    List<Position> selectByPrimaryKey(Integer id);

    List<Position> selectPsoitionByPsoitionCode(Integer psoitionCode);
}
