package com.lqkj.officialweb.dao.mapper;

import com.lqkj.officialweb.po.Required;
import com.lqkj.officialweb.response.ResultModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: zhen
 * @CreateTime: 2020/8/13 14:27
 */

@Mapper
public interface RequiredMapper
{
    List<Required> selectRequiredByPositionCode(@Param("positionCode") Integer positionCode);

}
