package com.lqkj.officialweb.dao.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.lqkj.officialweb.po.TNewsBanner;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface NewsBannerDao extends BaseMapper<TNewsBanner> {
    @Select("SELECT\n" +
            "	t.id, \n" +
            "	t.news_banner_img_url  , \n" +
            "	t.news_banner_img_id , \n" +
            "	t.news_banner_title , \n" +
            "	t.news_banner_details , \n" +
            "	t.remark, \n" +
            "	CASE \n" +
            "		WHEN t.is_used = '1' THEN  '是' \n" +
            "		WHEN t.is_used = '0' THEN  '否' \n" +
            "   end isUsed	\n" +
            "   FROM \n" +
            "	t_news_banner t \n" +
            "${ew.customSqlSegment}")
    public List<Map<String, Object>> getList(IPage<Map<String, Object>> page, @Param(Constants.WRAPPER) Wrapper<TNewsBanner> queryWrapper);
}