package com.lqkj.officialweb.dao.mapper;


import com.lqkj.officialweb.po.TNewsInformation;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface NewsInformationMapper {
    List<TNewsInformation> selectByNewsInformation(Integer newsCode);
}
