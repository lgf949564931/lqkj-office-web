package com.lqkj.officialweb.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: zhen
 * @CreateTime: 2020/8/13 14:22
 */
@Data
public class RequiredVo implements Serializable
{
    /**
     * ID
     */
    private Integer id;

    /**
     * 技能要求
     */
    private String position_required;

    /**
     * 技能编码
     */
    private String position_code;
}
