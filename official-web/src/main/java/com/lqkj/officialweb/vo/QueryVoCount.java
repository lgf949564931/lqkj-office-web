package com.lqkj.officialweb.vo;

import lombok.Data;

import java.util.List;

/**
 * @program: official-web
 * @description: 对查询到的数据和数量的一个封装类
 * @author: Ink_white
 * @create: 2020-08-06 11:43
 **/
@Data
public class QueryVoCount<T> {

    private int count;
    private List<T> pagelist;
}
